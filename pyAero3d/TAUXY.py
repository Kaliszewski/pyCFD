
import numpy as np

def TAUXY(u,v,mu,DX,DY,call_case):

# Calculate the xy (or yx) component of the shear stress.
    
    JMAX,IMAX=u.shape

    du_dy=np.zeros((JMAX,IMAX))
    dv_dx=np.zeros((JMAX,IMAX))

# Calculate the derivative of u wrt y and the derivative of v wrt x:
# For interior points we have a total of four cases combining the different finite difference schemes used to calculate du_dy and dv_dx.
# A the boundaries there is only one possibility: a forward difference when i,j = 1 and a rearward difference when i,j = IMAX,JMAX.
    if (call_case=='Predict_E') or (call_case=='Correct_E'):
        # The calculation of the derivative of u wrt y is the same for these two cases:
        for i in np.arange(0,IMAX):
            for j in np.arange(1,JMAX - 1):
                du_dy[j,i]=(u[j + 1,i] - u[j - 1,i]) / (2*DY)

        du_dy[0,:]=(u[1,:] - u[0,:]) / DY

        du_dy[JMAX-1,:]=(u[JMAX-1,:] - u[JMAX-2,:]) / DY

# The calculation of the derivative of v wrt x is different for these two cases:
        if (call_case=='Predict_E'):
# Rearward difference except at i = 1
            for i in np.arange(1,IMAX):
                for j in np.arange(0,JMAX):
                    dv_dx[j,i]=(v[j,i] - v[j,i - 1]) / DX

            dv_dx[:,0]=(v[:,1] - v[:,0]) / DX

        else:
# Forward difference except at i = IMAX
            for i in np.arange(0,IMAX-1):
                for j in np.arange(0,JMAX):
                    dv_dx[j,i]=(v[j,i + 1] - v[j,i]) / DX

            dv_dx[:,IMAX-1]=(v[:,IMAX-1] - v[:,IMAX-2]) / DX

    else:
        if (call_case=='Predict_F') or (call_case=='Correct_F'):
# The calculation of the derivative of v wrt x is the same for these two cases:
            for i in np.arange(1,IMAX - 1):
                for j in np.arange(0,JMAX):
                    dv_dx[j,i]=(v[j,i + 1] - v[j,i - 1]) / (np.dot(2,DX))

            dv_dx[:,0]=(v[:,1] - v[:,0]) / DX

            dv_dx[:,IMAX-1]=(v[:,IMAX-1] - v[:,IMAX-2]) / DX

# The calculation of the derivative of u wrt y is different for these two cases:
            if (call_case=='Predict_F'):
                # Rearward difference except at j = 1
                for i in np.arange(0,IMAX):
                    for j in np.arange(1,JMAX):
                        du_dy[j,i]=(u[j,i] - u[j - 1,i]) / DY

                du_dy[0,:]=(u[1,:] - u[0,:]) / DY
            else:
# Forward difference except at j = JMAX
                for i in np.arange(0,IMAX):
                    for j in np.arange(0,JMAX-1):
                        du_dy[j,i]=(u[j + 1,i] - u[j,i]) / DY

                du_dy[JMAX-1,:]=(u[JMAX-1,:] - u[JMAX-2,:]) / DY

        else:
            print('Undefined call case.')
    
# Finally, from the shear stress definition (assuming a Newtonian fluid) we have:
    tau_xy=np.dot(mu,(du_dy + dv_dx))
    return tau_xy
