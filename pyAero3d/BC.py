import numpy as np

def BC(rho,u,v,p,T,rho_inf,u_inf,p_inf,T_inf,T_w_T_inf,R,adiabatic_wall_flag):

# Apply boundary conditions (constant-temperature wall case).
    
    JMAX,IMAX=rho.shape
# We have four different cases:

# Case 1: Leading edge --> No-slip condition is enforced on velocity and the rest of the primitive flow field
# variables are assumed to be equal to their respective freestream values:
# u(1,1) = 0 and v(1,1) = 0 (implicit in the definition of u and v)
    T[0,0]=T_inf
    p[0,0]=p_inf
    rho[0,0]=rho_inf
# Case 2: Inflow / Upper boundary (not leading edge) --> y component of velocity (v) is assumed equal to zero
# and the rest of flow field properties are assumed to be their respective freestream values:
# For the inflow boundary:
    u[1:,0]=u_inf
    v[1:,0] = 0
    p[1:,0]=p_inf
    T[1:,0]=T_inf
    rho[1:,0]=rho_inf
# For the upper boundary:
    u[JMAX-1,1:]=u_inf
# v(JMAX,2:IMAX) = 0 is implicit in the definition of v
    p[JMAX-1,1:]=p_inf
    T[JMAX-1,1:]=T_inf
    rho[JMAX-1,1:]=rho_inf
# Case 4: Outflow (not surface or JMAX) --> Velocity, pressure and
# temperature are calculated based on an extrapolation from the two adjacent interior points.
# Density is computed from the equation of state:
    u[1:JMAX - 1,IMAX-1]=np.dot(2,u[1:JMAX - 1,IMAX - 1]) - u[1:JMAX - 1,IMAX - 2]
    v[1:JMAX - 1,IMAX-1]=np.dot(2,v[1:JMAX - 1,IMAX - 1]) - v[1:JMAX - 1,IMAX - 2]
    p[1:JMAX - 1,IMAX-1]=np.dot(2,p[1:JMAX - 1,IMAX - 1]) - p[1:JMAX - 1,IMAX - 2]
    T[1:JMAX - 1,IMAX-1]=np.dot(2,T[1:JMAX - 1,IMAX - 1]) - T[1:JMAX - 1,IMAX - 2]
    rho[1:JMAX - 1,IMAX-1]=np.divide(p[1:JMAX - 1,IMAX-1], (np.dot(R,T[1:JMAX - 1,IMAX-1])))
# Note: Case 4 must be executed before case 3 because the pressure
# extrapolation at IMAX needs the values of the outflow boundary

# Case 3: Surface (not leading edge) --> No-slip condition is specified on velocity.
# Temperature is either assumed to be equal to the wall temperature value
# or the adiabatic wall boundary condition is applied.
# Pressure is calculated by extrapolating from the values at the two points adjacent to the surface.
# Density is computed from the equation of state:
# u(1,2:IMAX) = 0 and v(1,2:IMAX) = 0 (implicit in the definition of u and v)
    if (adiabatic_wall_flag):
        T[0,1:IMAX-1]=T[1,1:IMAX-1]
# In the present case this condition is traduced in dT_dy = 0 at the wall.
    else:
        T[0,1:IMAX]=np.dot(T_w_T_inf,T_inf)

    p[0,1:IMAX]=np.dot(2,p[1,1:IMAX]) - p[2,1:IMAX]
    rho[0,1:IMAX]=p[0,1:IMAX] / (np.dot(R,T[0,1:IMAX]))

    return rho, u, v, p, T