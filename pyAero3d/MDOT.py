
import numpy as np


def MDOT(rho, u, rho_inf, u_inf, LVERT, DY,):

# Check the validity of the numerical solution by confirming conservation of mass.
# The continuity equation in integral form is applied to the converged (steady) flow field and the rate
# of mass inflow across the entrance of the computational domain is compared to the rate of mass outflow across the exit boundary.
# The solution is considered valid when the deviation between the mass flow rate at the entrance and exit is less than 1 percent.
    
    m_dot_in=np.dot(np.dot(- rho_inf,u_inf),(LVERT - DY)) - np.dot(np.dot(DY,rho_inf),u_inf) / 2
    
# both rho and u are constant except at the leading edge.
    
    y=np.arange(0,LVERT,DY)
    IMAX=rho.shape[1]
    m_dot_out=np.trapz(y,np.dot(rho[:,IMAX],u[:,IMAX]))
    deviation=np.dot(abs((m_dot_in + m_dot_out) / m_dot_in),100)
    if (deviation < 1):
        continuity=True
    else:
        continuity=False

    return continuity
    