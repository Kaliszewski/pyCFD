import numpy as np

def THERMC(Pr,c_p,mu,):

# Calculate thermal conductivity by means of the assumption of a constant Prandtl
# number for calorically perfect air.
    
    return np.multiply(mu, c_p / Pr)