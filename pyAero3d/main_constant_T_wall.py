from smop.core import *


#############
# Grid size #
#############
    
    IMAX=70
    JMAX=70
    MAXIT=10000.0
    t=1
    time=0

########################################
# Freestream conditions / Plate length #
########################################
    
# ISA sea level conditions
    M_inf=4
    a_inf=340.28
    p_inf=101325
    T_inf=288.16
    LHORI=1e-05

########################
# Constants definition #
########################
    
    T_w_T_inf=1

# wall boundary condition case).
    gamma=1.4
    mu_0=1.7894e-05
    T_0=288.16
    Pr=0.71
    R=287

# Compute mu_inf by means of Sutherland's law by using the DYNVIS function
# mu_inf = DYNVIS(T_inf, mu_0, T_0); # Freestream dynamic viscosity [kg/(m*s)]
    
# Continue with the definition of other necessary variables
    c_v=R / (gamma - 1)
    c_p=dot(gamma,c_v)
    rho_inf=p_inf / (dot(R,T_inf))
    Re_L=dot(dot(dot(rho_inf,M_inf),a_inf),LHORI) / DYNVIS(T_inf,mu_0,T_0)

    # e_inf = c_v*T_inf; # Freestream specific internal energy [J/kg]
    
# Compute k_inf by means of the constant Prandtl number assumption
# k_inf = THERMC(Pr, c_p, mu_inf); # Freestream thermal conductivity [W/(m*K)]
    
# Continue with the definition of other necessary variables
    delta=dot(5,LHORI) / sqrt(Re_L)

# Blasius calculation at the trailind edge [m]
    LVERT=dot(5,delta)

# Calculate step sizes in x and y
    DX=LHORI / (IMAX - 1)
    DY=LVERT / (JMAX - 1)
    x=arange(0,LHORI,DX)
    y=arange(0,LVERT,DY)
    CFL=0.6

#######################################
# Flow field variables initialization #
#######################################
    
# Only those variables that are needed in the calculation of the vectors U, E and F are initialized.
# The remaining flow properties can be obtained after as a postprocess to the solution.
    p=dot(ones(JMAX,IMAX),p_inf)
    rho=dot(ones(JMAX,IMAX),rho_inf)
    T=dot(ones(JMAX,IMAX),T_inf)
    T[1,:]=dot(T_w_T_inf,T_inf)

# wall boundary condition case)
    u=dot(dot(ones(JMAX,IMAX),M_inf),a_inf)
    u[1,:]=0
    v=zeros(JMAX,IMAX)
    mu=DYNVIS(T,mu_0,T_0)
    lambda_=dot(- 2 / 3,mu)
    k=THERMC(Pr,c_p,mu)

# Other variables needed for intermediate calculations are defined here:
    U1_p=zeros(JMAX,IMAX)
    U2_p=zeros(JMAX,IMAX)
    U3_p=zeros(JMAX,IMAX)
    U5_p=zeros(JMAX,IMAX)
    rho_p=zeros(JMAX,IMAX)
    u_p=zeros(JMAX,IMAX)
    v_p=zeros(JMAX,IMAX)
    T_p=zeros(JMAX,IMAX)
    p_p=zeros(JMAX,IMAX)
    # Start the main loop
    converged=copy(false)
    rho_old=copy(rho)
    while (logical_not(converged) and t <= MAXIT):

# The first thing to do is to compute the appropriate value of the time step needed to satisfy
# the CFL stability criterion. Only internal points are used in the calculation.
        v_prime=max(max(dot(dot(4 / 3,mu[2:JMAX - 1,2:IMAX - 1] ** 2),gamma) / (dot(Pr,rho[2:JMAX - 1,2:IMAX - 1]))))
        delta_t_CFL=1.0 / (abs(u[2:JMAX - 1,2:IMAX - 1]) / DX + abs(v[2:JMAX - 1,2:IMAX - 1]) / DY + dot(sqrt(dot(dot(gamma,R),T[2:JMAX - 1,2:IMAX - 1])),sqrt(1 / DX ** 2 + 1 / DY ** 2)) + dot(dot(2,v_prime),(1 / DX ** 2 + 1 / DY ** 2)))
        delta_t[t]=dot(CFL,min(min(delta_t_CFL)))

# The first stage is to compute the solution vector U (derivatives wrt t) from the primitive flow field variables
        U1=copy(rho)

        U2=multiply(rho,u)

        U3=multiply(rho,v)

        U5=multiply(rho,(dot(c_v,T) + (u ** 2 + v ** 2) / 2))

##################
# Predictor step #
##################
# On first place we have to calculate the flux vectors E (derivatives wrt x) and F (derivatives wrt y) from the primitive variables
        E1,E2,E3,E5=Primitive2E(rho,u,p,v,T,mu,lambda_,k,c_v,DX,DY,'Predict_E',nargout=4)
        F1,F2,F3,F5=Primitive2F(rho,u,p,v,T,mu,lambda_,k,c_v,DX,DY,'Predict_F',nargout=4)

# For interior points only:
# Forward differences
        for i in arange(2,IMAX - 1).reshape(-1):
            for j in arange(2,JMAX - 1).reshape(-1):
                U1_p[j,i]=U1[j,i] - dot(delta_t[t],((E1[j,i + 1] - E1[j,i]) / DX + (F1[j + 1,i] - F1[j,i]) / DY))
                U2_p[j,i]=U2[j,i] - dot(delta_t[t],((E2[j,i + 1] - E2[j,i]) / DX + (F2[j + 1,i] - F2[j,i]) / DY))
                U3_p[j,i]=U3[j,i] - dot(delta_t[t],((E3[j,i + 1] - E3[j,i]) / DX + (F3[j + 1,i] - F3[j,i]) / DY))
                U5_p[j,i]=U5[j,i] - dot(delta_t[t],((E5[j,i + 1] - E5[j,i]) / DX + (F5[j + 1,i] - F5[j,i]) / DY))

# Now, decode the flow field variables that will be needed for the calculation of predicted values of the flux vectors E and F.
# The variables needed are: rho_p, u_p, v_p, p_p, T_p, mu_p, lambda_p and k_p.
# For interior points we decode them from the U_p values predicted above:
        rho_p[2:JMAX - 1,2:IMAX - 1],u_p[2:JMAX - 1,2:IMAX - 1],v_p[2:JMAX - 1,2:IMAX - 1],T_p[2:JMAX - 1,2:IMAX - 1]=U2Primitive(U1_p[2:JMAX - 1,2:IMAX - 1],U2_p[2:JMAX - 1,2:IMAX - 1],U3_p[2:JMAX - 1,2:IMAX - 1],U5_p[2:JMAX - 1,2:IMAX - 1],c_v,nargout=4)
        p_p[2:JMAX - 1,2:IMAX - 1]=multiply(dot(rho_p[2:JMAX - 1,2:IMAX - 1],R),T_p[2:JMAX - 1,2:IMAX - 1])
        rho_p,u_p,v_p,p_p,T_p=BC(rho_p,u_p,v_p,p_p,T_p,rho_inf,dot(M_inf,a_inf),p_inf,T_inf,T_w_T_inf,R,false,nargout=5)
        mu_p=DYNVIS(T_p,mu_0,T_0)
        lambda_p=dot(- 2 / 3,mu_p)
        k_p=THERMC(Pr,c_p,mu_p)

# Now we move to the corrector step:
##################
# Corrector step #
##################
# As before, first of all we have to calculate the flux vectors E (derivatives wrt x) and F (derivatives wrt y) from the primitive variables.
# In this case from the predicted variables we have:
        E1_p,E2_p,E3_p,E5_p=Primitive2E(rho_p,u_p,p_p,v_p,T_p,mu_p,lambda_p,k_p,c_v,DX,DY,'Correct_E',nargout=4)
        F1_p,F2_p,F3_p,F5_p=Primitive2F(rho_p,u_p,p_p,v_p,T_p,mu_p,lambda_p,k_p,c_v,DX,DY,'Correct_F',nargout=4)
# For interior points only:
# Rearward differences
        for i in arange(2,IMAX - 1).reshape(-1):
            for j in arange(2,JMAX - 1).reshape(-1):
                U1[j,i]=dot(1 / 2,(U1[j,i] + U1_p[j,i] - dot(delta_t[t],((E1_p[j,i] - E1_p[j,i - 1]) / DX + (F1_p[j,i] - F1_p[j - 1,i]) / DY))))
                U2[j,i]=dot(1 / 2,(U2[j,i] + U2_p[j,i] - dot(delta_t[t],((E2_p[j,i] - E2_p[j,i - 1]) / DX + (F2_p[j,i] - F2_p[j - 1,i]) / DY))))
                U3[j,i]=dot(1 / 2,(U3[j,i] + U3_p[j,i] - dot(delta_t[t],((E3_p[j,i] - E3_p[j,i - 1]) / DX + (F3_p[j,i] - F3_p[j - 1,i]) / DY))))
                U5[j,i]=dot(1 / 2,(U5[j,i] + U5_p[j,i] - dot(delta_t[t],((E5_p[j,i] - E5_p[j,i - 1]) / DX + (F5_p[j,i] - F5_p[j - 1,i]) / DY))))

# Finally, decode the corrected flow field variables.
# For interior points we decode them from the corrected U values obtained above:
        rho[2:JMAX - 1,2:IMAX - 1],u[2:JMAX - 1,2:IMAX - 1],v[2:JMAX - 1,2:IMAX - 1],T[2:JMAX - 1,2:IMAX - 1]=U2Primitive(U1[2:JMAX - 1,2:IMAX - 1],U2[2:JMAX - 1,2:IMAX - 1],U3[2:JMAX - 1,2:IMAX - 1],U5[2:JMAX - 1,2:IMAX - 1],c_v,nargout=4)
        p[2:JMAX - 1,2:IMAX - 1]=multiply(dot(rho[2:JMAX - 1,2:IMAX - 1],R),T[2:JMAX - 1,2:IMAX - 1])
        rho,u,v,p,T=BC(rho,u,v,p,T,rho_inf,dot(M_inf,a_inf),p_inf,T_inf,T_w_T_inf,R,false,nargout=5)
        mu=DYNVIS(T,mu_0,T_0)
        lambda_=dot(- 2 / 3,mu)
        k=THERMC(Pr,c_p,mu)

# At this point we end the MacCormack's algorithm.
#####################
# Convergence check #
#####################
#  Check for convergence before advancing to the next iteration (include NaN, complex check etc.)
        converged=CONVER(rho_old,rho)
        rho_old=copy(rho)
        time=time + delta_t[t]
        t=t + 1

    if (converged):
        continuity=MDOT(rho,u,rho_inf,dot(M_inf,a_inf),LVERT,DY)
    else:
        error('CALCULATION FAILED: the maximum number of iterations has been reached before achieving convergence.')
    if (continuity):
        disp('The calculation for M = 4 with a constant-temperature wall boundary condition has finished successfully.')
    else:
        error('The solution has converged to an invalid result.')
    
    M=sqrt(u ** 2 + v ** 2) / sqrt(dot(dot(gamma,R),T))
    figure
    surf(x,y,M)
    view(2)
    axis('equal')
    colorbar
    title('Mach number (constant-temperature wall)')
    xlabel('x')
    ylabel('y')