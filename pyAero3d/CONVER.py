import numpy as np
    

def CONVER(rho_old,rho):

# Check for a converged solution. The convergence criterion is that the
# change in density between time steps is lower than 1e-14.
    
    if (np.iscomplex(rho).any()):
        print('The calculation has failed. A complex number has been detected.')
    else:
        if ((np.max(abs(rho_old - rho))) < 1e-08):
            return True

    return False

    