
import numpy as np

def QY(T,k,DY,call_case):

# Calculate the y-component of the heat flux vector.
    
    JMAX,IMAX=T.shape
    dT_dy=np.zeros((JMAX,IMAX))
# Calculate the derivative of temperature wrt y:
# forward difference for j = 1 and a rearward difference when j = JMAX
    if (call_case=='Predict_F'):
        for i in np.arange(0,IMAX):
            for j in np.arange(1,JMAX):
                dT_dy[j,i]=(T[j,i] - T[j - 1,i]) / DY
        dT_dy[0,:]=(T[1,:] - T[0,:]) / DY
    else:
        if (call_case=='Correct_F'):
            for i in np.arange(0,IMAX):
                for j in np.arange(0,JMAX - 1):
                    dT_dy[j,i]=(T[j + 1,i] - T[j,i]) / DY
            dT_dy[JMAX-1,:]=(T[JMAX-1,:] - T[JMAX - 2,:]) / DY
        else:
            print('Undefined call case.')
    
# Then, following Fourier's law for heat conduction we have:
    q_y=np.dot(- k,dT_dy)

    return q_y