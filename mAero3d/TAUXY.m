
function tau_xy = TAUXY(u, v, mu, DX, DY, call_case)

[JMAX, IMAX] = size(u);
du_dy = zeros(JMAX, IMAX);
dv_dx = zeros(JMAX, IMAX);

if (strcmp(call_case, 'Predict_E') || strcmp(call_case, 'Correct_E'))
 
    for i = 1:IMAX
        for j = 2:JMAX-1
            du_dy(j,i) = (u(j+1,i) - u(j-1,i))/(2*DY); 
        end
    end
    du_dy(1,:) = (u(2,:) - u(1,:))/DY; 
    du_dy(JMAX,:) = (u(JMAX,:) - u(JMAX-1,:))/DY; 
    
    if (strcmp(call_case, 'Predict_E'))

        for i = 2:IMAX
            for j = 1:JMAX
                dv_dx(j,i) = (v(j,i) - v(j,i-1))/DX;
            end
        end
        dv_dx(:,1) = (v(:,2) - v(:,1))/DX; 
    else

        for i = 1:IMAX-1
            for j = 1:JMAX
                dv_dx(j,i) = (v(j,i+1) - v(j,i))/DX;
            end
        end
        dv_dx(:,IMAX) = (v(:,IMAX) - v(:,IMAX-1))/DX;
    end
elseif (strcmp(call_case, 'Predict_F') || strcmp(call_case, 'Correct_F'))

    for i = 2:IMAX-1
        for j = 1:JMAX
            dv_dx(j,i) = (v(j,i+1) - v(j,i-1))/(2*DX); 
        end
    end
    dv_dx(:,1) = (v(:,2) - v(:,1))/DX;
    dv_dx(:,IMAX) = (v(:,IMAX) - v(:,IMAX-1))/DX; 
    
    if (strcmp(call_case, 'Predict_F'))

        for i = 1:IMAX
            for j = 2:JMAX
                du_dy(j,i) = (u(j,i) - u(j-1,i))/DY;
            end
        end
        du_dy(1,:) = (u(2,:) - u(1,:))/DY; 
    else
 
        for i = 1:IMAX
            for j = 1:JMAX-1
                du_dy(j,i) = (u(j+1,i) - u(j,i))/DY;
            end
        end
        du_dy(JMAX,:) = (u(JMAX,:) - u(JMAX-1,:))/DY; 
    end
else
    error('Undefined call case.')
end

tau_xy = mu.*(du_dy + dv_dx);