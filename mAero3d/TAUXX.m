
function tau_xx = TAUXX(u, v, lambda, mu, DX, DY, call_case)


[JMAX, IMAX] = size(u);
du_dx = zeros(JMAX, IMAX);
dv_dy = zeros(JMAX, IMAX);

if (strcmp(call_case, 'Predict_E'))
    for i = 2:IMAX
        for j = 1:JMAX
            du_dx(j,i) = (u(j,i) - u(j,i-1))/DX;
        end
    end
    du_dx(:,1) = (u(:,2) - u(:,1))/DX; 
elseif (strcmp(call_case, 'Correct_E'))
    for i = 1:IMAX-1
        for j = 1:JMAX
            du_dx(j,i) = (u(j,i+1) - u(j,i))/DX; 
        end
    end
    du_dx(:,IMAX) = (u(:,IMAX) - u(:,IMAX-1))/DX;
else
    error('Undefined call case.')
end


for i = 1:IMAX
    for j = 2:JMAX-1
        dv_dy(j,i) = (v(j+1,i) - v(j-1,i))/(2*DY);
    end
end
dv_dy(1,:) = (v(2,:) - v(1,:))/DY; 
dv_dy(JMAX,:) = (v(JMAX,:) - v(JMAX-1,:))/DY;


tau_xx = lambda.*(du_dx + dv_dy) + 2*mu.*du_dx;