This Matlab code simulates supersonic flow over a flat plate by solving the complete Navier-Stokes equations in laminar regime. The code structure and the numerical algorithm follow the description given in chapter 10 of Anderson's CFD book (John D. Anderson, JR. - Computational Fluid Dynamics. The Basics with Applications). A time-marching finite difference method based on MacCormack's predictor-corrector technique is used for the solution and discretization. Both the constant temperature and the adiabatic wall cases can be solved. There is also a script that generates the same plots as those in the book for comparison.

- To calculate the constant-temperature wall solution run the file main_constant_T_wall.m
- To compute the adiabatic wall solution run main_adiabatic_wall.m
- The script plots_generator.m runs both cases and generates plots for comparison with the graphs of the book.

In the original configuration, the constant-temperature wall case takes 2497 time steps to converge, and the adiabatic wall case 3957.