function [rho, u, v, p, T] = BC(rho, u, v, p, T, rho_inf, u_inf, p_inf, T_inf, T_w_T_inf, R, adiabatic_wall_flag)

[JMAX, IMAX] = size(rho);

T(1,1) = T_inf;
p(1,1) = p_inf;
rho(1,1) = rho_inf;

u(2:JMAX,1) = u_inf;

p(2:JMAX,1) = p_inf;
T(2:JMAX,1) = T_inf;
rho(2:JMAX,1) = rho_inf;


u(JMAX,2:IMAX) = u_inf;

p(JMAX,2:IMAX) = p_inf;
T(JMAX,2:IMAX) = T_inf;
rho(JMAX,2:IMAX) = rho_inf;


u(2:JMAX-1,IMAX) = 2*u(2:JMAX-1,IMAX-1) - u(2:JMAX-1,IMAX-2);
v(2:JMAX-1,IMAX) = 2*v(2:JMAX-1,IMAX-1) - v(2:JMAX-1,IMAX-2);
p(2:JMAX-1,IMAX) = 2*p(2:JMAX-1,IMAX-1) - p(2:JMAX-1,IMAX-2);
T(2:JMAX-1,IMAX) = 2*T(2:JMAX-1,IMAX-1) - T(2:JMAX-1,IMAX-2);
rho(2:JMAX-1,IMAX) = p(2:JMAX-1,IMAX)./(R*T(2:JMAX-1,IMAX));

if (adiabatic_wall_flag) % Then an adiabatic wall is assumed
    T(1,2:IMAX) = T(2,2:IMAX); % This condition results from enforcing that the normal temperature gradient pointing into to the wall is zero.
    % In the present case this condition is traduced in dT_dy = 0 at the wall.
else
    T(1,2:IMAX) = T_w_T_inf*T_inf;
end
p(1,2:IMAX) = 2*p(2,2:IMAX) - p(3,2:IMAX);
rho(1,2:IMAX) = p(1,2:IMAX)./(R*T(1,2:IMAX));