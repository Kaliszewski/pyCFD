
function converged = CONVER(rho_old, rho)


if (~isreal(rho))
    error('The calculation has failed. A complex number has been detected.')
elseif (max(max(abs(rho_old - rho))) < 1e-8)
    converged = true;
else
    converged = false;
end