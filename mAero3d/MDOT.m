

function continuity = MDOT(rho, u, rho_inf, u_inf, LVERT, DY)

m_dot_in = - rho_inf*u_inf*(LVERT - DY) - DY*rho_inf*u_inf/2; 

y = 0:DY:LVERT;
IMAX = size(rho,2);
m_dot_out = trapz(y, rho(:,IMAX).*u(:,IMAX));

deviation = abs((m_dot_in + m_dot_out)/m_dot_in)*100;
if (deviation < 1)
    continuity = true;
else
    continuity = false;
end