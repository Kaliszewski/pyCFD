
function tau_yy = TAUYY(u, v, lambda, mu, DX, DY, call_case)

[JMAX, IMAX] = size(v);
du_dx = zeros(JMAX, IMAX);
dv_dy = zeros(JMAX, IMAX);

if (strcmp(call_case, 'Predict_F'))
    for i = 1:IMAX
        for j = 2:JMAX
            dv_dy(j,i) = (v(j,i) - v(j-1,i))/DY; 
        end
    end
    dv_dy(1,:) = (v(2,:) - v(1,:))/DY; 
elseif (strcmp(call_case, 'Correct_F'))
    for i = 1:IMAX
        for j = 1:JMAX-1
            dv_dy(j,i) = (v(j+1,i) - v(j,i))/DY; 
        end
    end
    dv_dy(JMAX,:) = (v(JMAX,:) - v(JMAX-1,:))/DY; 
else
    error('Undefined call case.')
end

for i = 2:IMAX-1
    for j = 1:JMAX
        du_dx(j,i) = (u(j,i+1) - u(j,i-1))/(2*DX);
    end
end
du_dx(:,1) = (u(:,2) - u(:,1))/DX; 
du_dx(:,IMAX) = (u(:,IMAX) - u(:,IMAX-1))/DX; 

tau_yy = lambda.*(du_dx + dv_dy) + 2*mu.*dv_dy;